
Pod::Spec.new do |s|

  s.name         = "TACNotifications"
  s.version      = "0.0.3"
  s.summary      = "Gamooga iOS SDK"
  s.description  = "Gamooga iOS SDK"
  s.homepage     = "https://gitlab.com/gamooga-ios/TACNotificationsFramework.git"

  s.license      = "Commercial"
  s.author       = "Gamooga"
  s.platform     = :ios, "10.0"

  s.source       = { :path => 'https://gitlab.com/gamooga-ios/TACNotificationsFramework.git' }

  s.vendored_frameworks = "TACNotifications.framework"
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4' }

end
