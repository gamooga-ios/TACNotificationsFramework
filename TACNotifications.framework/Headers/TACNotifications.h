//
//  TACNotifications.h
//  TACNotifications
//
//  Created by Gamooga on 01/12/16.
//  Copyright © 2016 Gamooga. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>

//! Project version number for TACNotifications.
FOUNDATION_EXPORT double TACNotificationsVersionNumber;

//! Project version string for TACNotifications.
FOUNDATION_EXPORT const unsigned char TACNotificationsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TACNotifications/PublicHeader.h>

@interface TACNotifications : NSObject

- (void)serviceExtensionTimeWillExpire;
- (void)didReceiveNSRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler;
- (TACNotifications *)init;
- (NSInteger)didReceiveVCNotification:(UNNotification *)notification;
- (void)didReceiveVCResponse:(UNNotificationResponse *)response completionHandler:(void (^)(UNNotificationContentExtensionResponseOption, NSURL *, NSInteger))completion;
- (NSInteger)numberOfItems;
- (UIView *)carousel:(NSInteger)index reusingView:(UIView *)view;
- (CGFloat)carouselItemWidth;
- (void)setTotalWidth:(CGFloat)totalWidth;

@end
